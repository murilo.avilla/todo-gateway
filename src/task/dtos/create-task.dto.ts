import {
  IsIn,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export enum TaskStatus {
  PENDING = 'PENDING',
  STARTED = 'STARTED',
  RUNNING = 'RUNNING',
  COMPLETED = 'COMPLETED',
  ABANDONED = 'ABANDONED',
}

export class CreateTaskDTO {
  @IsNotEmpty()
  @IsString()
  collectionId: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNumber()
  @IsOptional()
  order: number;

  @IsOptional()
  @IsIn([
    TaskStatus.PENDING,
    TaskStatus.STARTED,
    TaskStatus.RUNNING,
    TaskStatus.COMPLETED,
  ])
  status: TaskStatus;
}
