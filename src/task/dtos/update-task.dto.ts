import { IsIn, IsNumber, IsOptional, IsString } from 'class-validator';
import { TaskStatus } from './create-task.dto';

export class UpdateTaskDTO {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  order: number;

  @IsOptional()
  @IsIn([
    TaskStatus.PENDING,
    TaskStatus.STARTED,
    TaskStatus.RUNNING,
    TaskStatus.COMPLETED,
  ])
  status: TaskStatus;
}
