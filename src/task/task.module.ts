import { Module } from '@nestjs/common';
import { TaskController } from './task.controller';
import { ProxyrmqModule } from 'src/proxyrmq/proxyrmq.module';

@Module({
  controllers: [TaskController],
  imports: [ProxyrmqModule],
})
export class TaskModule {}
