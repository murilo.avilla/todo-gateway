import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ClientProxyTodo } from 'src/proxyrmq/client-proxy.provider';
import { CreateTaskDTO } from './dtos/create-task.dto';
import { Request } from 'express';
import { Observable, firstValueFrom } from 'rxjs';
import { UpdateTaskDTO } from './dtos/update-task.dto';

@Controller('v1/task')
@UseGuards(JwtAuthGuard)
export class TaskController {
  constructor(private readonly clientProxy: ClientProxyTodo) {}

  private readonly clientTodoBackend: ClientProxy =
    this.clientProxy.getClientProxyAdminBackend();

  @Post()
  @UsePipes(ValidationPipe)
  async createTask(@Body() createTaskDTO: CreateTaskDTO, @Req() req: Request) {
    const user: any = req?.user;

    return this.clientTodoBackend.emit('task-create', {
      user: user.sub,
      ...createTaskDTO,
    });
  }

  @Get('/:collectionId')
  findTask(
    @Req() req: Request,
    @Param('collectionId') collectionId: string,
    @Query('taskId') _id: string,
  ): Observable<any> {
    const user: any = req?.user;
    return this.clientTodoBackend.send('task-find', {
      userId: user.sub,
      collectionId,
      _id,
    });
  }

  @Put('/:taskId')
  async updateTask(
    @Param('collectionId') collectionId: string,
    @Param('taskId') _id: string,
    @Req() req: Request,
    @Body() updateTaskDTO: UpdateTaskDTO,
  ) {
    const user: any = req?.user;
    const task: any[] = await firstValueFrom(
      this.clientTodoBackend.send('task-find', {
        userId: user?.sub,
        _id,
      }),
    );

    if (task.length <= 0)
      throw new BadRequestException('Task not found in collection');

    return this.clientTodoBackend.emit('task-update', {
      taskId: _id,
      task: updateTaskDTO,
    });
  }
}
