import { Module } from '@nestjs/common';
import { ClientProxyTodo } from './client-proxy.provider';

@Module({
  providers: [ClientProxyTodo],
  exports: [ClientProxyTodo],
})
export class ProxyrmqModule {}
