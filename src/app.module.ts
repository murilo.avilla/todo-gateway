import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './auth/jwt.strategy';
import { CollectionModule } from './collection/collection.module';
import { TaskModule } from './task/task.module';
import { ProxyrmqModule } from './proxyrmq/proxyrmq.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule,
    ProxyrmqModule,
    CollectionModule,
    TaskModule,
  ],
  controllers: [AppController],
  providers: [JwtStrategy],
})
export class AppModule {}
