import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class UpdateCollectionDTO {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsBoolean()
  status: boolean;
}
