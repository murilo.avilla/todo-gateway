import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCollectionDTO {
  @IsNotEmpty()
  @IsString()
  name: string;
}
