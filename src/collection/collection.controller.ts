import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

import { ClientProxyTodo } from 'src/proxyrmq/client-proxy.provider';
import { Request } from 'express';
import { Observable, firstValueFrom } from 'rxjs';
import { CreateCollectionDTO } from './dtos/create-collection.dto';
import { UpdateCollectionDTO } from './dtos/update-collection.dto';

@Controller('v1/collection')
@UseGuards(JwtAuthGuard)
export class CollectionController {
  constructor(private readonly clientProxy: ClientProxyTodo) {}

  private clientTodoBackend: ClientProxy =
    this.clientProxy.getClientProxyAdminBackend();

  @Post()
  @UsePipes(ValidationPipe)
  async createCollection(
    @Body() createCollecttionDto: CreateCollectionDTO,
    @Req() req: Request,
  ) {
    const user: any = req?.user;

    return this.clientTodoBackend.emit('collection-create', {
      user: user.sub,
      ...createCollecttionDto,
    });
  }

  @Get()
  findCollections(@Req() req: Request): Observable<any> {
    const user: any = req?.user;
    return this.clientTodoBackend.send('collection-find', { userId: user.sub });
  }

  @Put('/:id')
  async updateCollection(
    @Param('id') id: string,
    @Req() req: Request,
    @Body() updateCollectionDTO: UpdateCollectionDTO,
  ) {
    const user: any = req?.user;
    const collectionFound = await firstValueFrom(
      this.clientTodoBackend.send('collection-find', {
        userId: user.sub,
        collectionId: id,
      }),
    );

    if (collectionFound.length <= 0)
      throw new BadRequestException('Collection not found');

    return this.clientTodoBackend.emit('collection-update', {
      user: user.sub,
      collectionId: id,
      collection: updateCollectionDTO,
    });
  }
}
