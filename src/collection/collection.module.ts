import { Module } from '@nestjs/common';
import { CollectionController } from './collection.controller';
import { ProxyrmqModule } from 'src/proxyrmq/proxyrmq.module';

@Module({
  controllers: [CollectionController],
  imports: [ProxyrmqModule],
})
export class CollectionModule {}
